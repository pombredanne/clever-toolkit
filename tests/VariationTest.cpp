#include <boost/test/unit_test.hpp>

#include <vector>
#include <boost/unordered_set.hpp>
#include "../src/Variation.h"

using namespace std;

BOOST_AUTO_TEST_CASE( VariationTest )
{
	boost::unordered_set<Variation> s;
	s.insert(Variation("chr1", 10001,   120, -1.0, Variation::INSERTION));
	s.insert(Variation("chr1", 10001,   120, -1.0, Variation::INSERTION));
	s.insert(Variation("chr2", 10001,   120, -1.0, Variation::INSERTION));
	s.insert(Variation("chr1", 10001,   120, -1.0, Variation::DELETION));
	s.insert(Variation("chr1", 100,   120, -1.0, Variation::DELETION));
	s.insert(Variation("chr1", 100,   180, -1.0, Variation::DELETION));
	s.insert(Variation("chr2", 10001,   120, -1.0, Variation::INSERTION));
	
	BOOST_CHECK_EQUAL(s.size(), 5);
//	BOOST_CHECK(query9.get() == 0);
	
	Variation v("chr1", 10001,   120, -1.0, Variation::INSERTION);
	v.clear();
	BOOST_CHECK_EQUAL(v.getType(), Variation::NONE);
}
