#include <boost/test/unit_test.hpp>

#include <vector>

#include "../src/SumOfBinomials.h"

using namespace std;

BOOST_AUTO_TEST_CASE( SumOfBinomialsTest ) {
	SumOfBinomials s;
	s.add(0.5, 4);
	BOOST_CHECK_CLOSE( s.probability(0), 0.0625, 1e-10 );
	BOOST_CHECK_CLOSE( s.probability(1), 0.25, 1e-10 );
	BOOST_CHECK_CLOSE( s.probability(2), 0.375, 1e-10 );
	BOOST_CHECK_CLOSE( s.probability(3), 0.25, 1e-10 );
	BOOST_CHECK_CLOSE( s.probability(4), 0.0625, 1e-10 );
	s.add(0.5, 2);
	BOOST_CHECK_CLOSE( s.probability(0), 0.015625, 1e-10 );
	BOOST_CHECK_CLOSE( s.probability(1), 0.09375, 1e-10 );
	BOOST_CHECK_CLOSE( s.probability(2), 0.234375, 1e-10 );
	BOOST_CHECK_CLOSE( s.probability(3), 0.312500, 1e-10 );
	BOOST_CHECK_CLOSE( s.probability(4), 0.234375, 1e-10 );
	BOOST_CHECK_CLOSE( s.probability(5), 0.09375, 1e-10 );
	BOOST_CHECK_CLOSE( s.probability(6), 0.015625, 1e-10 );
	SumOfBinomials s2;
	s2.add(0.1,1);
	s2.add(0.6,1);
	BOOST_CHECK_CLOSE( s2.probability(0), 0.9*0.4, 1e-10 );
	BOOST_CHECK_CLOSE( s2.probability(1), 0.1*0.4+0.9*0.6, 1e-10 );
	BOOST_CHECK_CLOSE( s2.probability(2), 0.1*0.6, 1e-10 );
}
